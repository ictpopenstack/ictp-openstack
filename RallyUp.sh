#!/bin/bash

#RalluUp Version: 1.3
#OpenStack and Rally Deployment
#Script Development and Reseach by:Martin Shepherd: u903588@uni.canberra.edu.au, Jason Day U3092444@uni.canberra.edu.au, Brendon Stephens U3136310@uni.canberra.edu.au, Ayo Ogunsowo U3136606@uni.canberra.edu.au
#This script utilizes resources from the "OpenStack Foundation" which carries the  Apache License, Version 2.0 (ASLv2).

#This script runs by default in interactive mode, where a basic user interface guides the install.
#Alternatively, segments can be run directly through use of the following command line parameters...

#Command Line Parameter use: RallyUp.sh [parameter1] [parameter2] [parameter3] [parameter4]
#[parameter1] - test to perform
# -i (or no Parameter): Interactive Mode (normal dialodue box)
# -o Complete install of OpenStack using 'DevStack' (same as option 1 in UI)
# -v VirtualInstanceInstall which will instantiate a Linux machine on OpenStack. (same as option 2 in UI)
# -r Rally install: Rally is installed and the default tests run (same as option 3 in UI)
# -c Continuous monitoring tests are run: Rally install with default continuous monitoring tests applied (same as option 4 in UI)
# -a All-In-One install of Devstack and Rally and then a test. (same as optio5 4 in UI)
# -h To view the Latest HTML Rally report in firefox

#Note:
# To customise the -c you can add further switches, which must be placed in the following order...

#[parameter2] - test to perform - 
#-number (1 to 5) or final section of path to json benchmarking file ( after /opt/stack/devstack/rally-openstack/samples )
#-Test Bank to carry out (1 - 5)

#[parameter3]
#-Separation between tests in seconds

#[parameter4]
# 
#-Number of repeats (For infinite repeats, put 0)

#Example:
# The default without altering any parameters would be the same as using RallyUp.sh -c 1 15 5
#-Tip: If you want to use the UI, but alter the repeats or separation, launch RallyUp similar to: RallyUp.sh -i 4 5 3

#Changelog for 1.0
#-Minor punctuation changes

#Changelog for 1.1
#-Tasks made individual, for example installing OpenStack and creating a Virtual Instance with Linux are now two tasks 
#-Intermediate functions created to launch tasks so business logic more contained
#-Tests throughout to detect if OpenStack or Rally are already installed (so does not attempt again)
#-Added selection which launches the latest Rally test in a browser to view
#-Added 5 default Rally tests to choose between, plus a way to launch any of the tests by substituting the $RALLYTESTBANK parameter with part of the path
#-Added a way to change which test is the default one for use in the interactive control dialogue

#Changelog for 1.2
#-Bug with check for stack user was stopping deployment, as stack user installed just prior
#-All checks utilizing folder paths removed and checks testing for OpenStack or Rally versions implemented instead
#-Dialogue box text size dropped a notch
#-Rally deployment was broken, with plugins retrievel breaking due to incompatability of the 'GIT' rally deployment with the 'pypi' plugin deployment.
#	As 'pypi' also does deployment, the 'GIT' deployment has been removed, along with the (activated) Virtual Session it utilised.
#-Rally deployment script broken up with labels in echo's
#-Added RallyViewReport to the end of OpenStackRallyThenTest

#Changelog for 1.3
#- Removed ; sudo pip install --upgrade pip line 425 as it effected non-VM Linux hosts with later Rally install failing
#-Added detection for JSON file in file selector
#-Fixed issue where on secound time around pressing cancel breaks the Rally Test input box. Case statement installed to handle return of 1 (cancel)
#	and now variable gets a value (it got null before)
#-Improved parameter descriptions

#Bash specific commands
#To turn off case sensetive matching, so when checking JSON extension will not fail with json
echo "Setting 'shopt' to not match case for when comparing file extensions"
shopt -s nocasematch

#CONSTANTS
#^^^^^^^^^
#-----------------------------------------------------------------------------
#Testmode returns, rather than running through tasks. (For faultfinding logic)
#-----------------------------------------------------------------------------
TESTMODE=0;

#------------------------------------------------------------------
#Constants used in checks for if installations have already occured
#------------------------------------------------------------------
OSDIRECTORYDEVSTACK="/opt/stack";
OSDIRECTORYANSIBLESTACK="/opt/openstack-ansible";

#------------------------------------------------------------------
#Variable for the names of the default JSON test files
#------------------------------------------------------------------
RallyDefaultJSONFilePathBeginning="/opt/stack/devstack/rally-openstack/samples"
RallyDefaultJSONFile1="/scenarios/nova/boot-and-delete.json"
RallyDefaultJSONFile2="/sla/create-and-delete-user.json"
RallyDefaultJSONFile3="/scenarios/glance/create-and-delete-image.json"
RallyDefaultJSONFile4="/scenarios/neutron/create-and-delete-networks.json"
RallyDefaultJSONFile5="/scenarios/cinder/create-and-delete-snapshot.json"

#Parameter management for Continuous Testing section, should they not be specified through added switches when calling script. 
if [ "$2" != "" ]; then
	RALLYTESTBANK=$2
	echo hello
else
    RALLYTESTBANK=1 ; #Use the default test bank
fi
if [ "$3" != "" ]; then
    RALLYTESTSEPARATION=$3
else
    RALLYTESTSEPARATION=15 ; #Use the default test separation in seconds
fi
if [ "$4" != "" ]; then
    RALLYTESTREPEATS=$4
else
    RALLYTESTREPEATS=5 ; #Use the default number of repeats
fi


#----------------------------------------------------------------------------------------------
#Choice of 5 Default Test management tasks, driven from RALLYTESTBANK parameter
#The case statement will append to path /opt/stack/devstack/rally-openstack/samples/tasks
#If you want to choose your own test, use a continued path like those below instead of a number
#For example use: . RallyUp.sh -c /scenarios/cinder/create-and-delete-snapshot.json 5 2
#This will run the snapshot test, wait 5 seconds and then run it a second time.
#----------------------------------------------------------------------------------------------
DefineRallyDefaultTest(){
case "$RALLYTESTBANK" in
	1) RallyJSONFile=${RallyDefaultJSONFile1} ;;
	2) RallyJSONFile=${RallyDefaultJSONFile2} ;;
	3) RallyJSONFile=${RallyDefaultJSONFile3} ;;
	4) RallyJSONFile=${RallyDefaultJSONFile4} ;;
    5) RallyJSONFile=${RallyDefaultJSONFile5} ;;
    *) RallyJSONFile="$RALLYTESTBANK" ;; #If RALLYTESTBANK is not a number, it can be a continuation of a path appended to above comment's path
esac
}
#Run the DefineRallyDefaultTest function
DefineRallyDefaultTest

#----------------------------------------------------------------------------------------------------------------------------------------------------------------
#testdialog=$(zenity --width 600 --height 100 --entry --title "Zenity Test" --text="`printf "For some reason the Zenity dialog\n doesn't want to linefeed.\n Any suggestions?"`")
#echo $testdialog

#Functions must go before calls to them, so this first function called by parameter 'Case' statement at the end of script
#------------------------------------------------------------------
#Launch Zenity interface for interactive or non-interactive install
#------------------------------------------------------------------
InteractiveInstall(){
	echo This will launch interactive OS and Rally install UI
	title="RallyUp Interactive OpenStack install and Rally Benchmarking."
	prompt="Select an action for interactive install or benchmarking tests:"
	options=("1. Install OpenStack using DevStack" "2. Install a Virtual Instance of a Linux machine on OpenStack" "3. Install Rally (if not installed) and then perform the default benchmarking test" "4. Install Rally (if not installed) for continuous monitoring (benchmarking) of OpenStack" "5. Install OpenStack then Install Rally (if not installed) and perform the default benchmarking test" "6. Generate and View the latest Rally report in a browser" "7. Change the default benchmarking test Rally uses")

	#Launch a Zenity User Interface Listbox
	#Standard errors need to be handled by appending 2>/dev/null to each Zennity command.. (?Bug/Feature) - see: https://www.linuxquestions.org/questions/debian-26/%5Bsolved%5D-gtk-message-gtkdialog-mapped-without-a-transient-parent-this-is-discouraged-4175550278/
	while opt=$(zenity --title="$title" --text="$prompt" --list  --width=600  --height=300 --column="Options" "${options[@]}" 2>/dev/null); do
		case "$opt" in
		"${options[0]}" ) ZenityMessageDEVSTACK 2>/dev/null;
							if [[ $ZenityMessageDEVSTACK_ret == 0 ]]; then
								OpenStackInstallViaDevStack ;
							else
								echo "user pressed cancel" ;
							fi ;;
		"${options[1]}" ) ZenityMessageVirtualInstance 2>/dev/null
							if [[ $ZenityMessageVirtualInstance_ret == 0 ]]; then
								VirtualInstanceInstall ;
							else
								echo "user pressed cancel" ;
							fi ;;
		"${options[2]}" ) ZenityMessageRallyWithTest 2>/dev/null
							if [[ $ZenityMessageRallyWithTest_ret == 0 ]]; then
								RallyInstallWithTest ;
							else
								echo "user pressed cancel" ;
							fi ;;
		"${options[3]}" ) ZenityMessageRallyContinuous 2>/dev/null
							if [[ $ZenityMessageRallyContinuous_ret == 0 ]]; then
								RallyInstallWithContinuousTesting ;
							else
								echo "user pressed cancel" ;
							fi ;;
		"${options[4]}" ) ZenityMessageRallyDEVSTACKandTest 2>/dev/null 
							if [[ $ZenityMessageRallyDEVSTACKandTest_ret == 0 ]]; then
								OpenStackRallyThenTest;
							else
								echo "user pressed cancel" ;
							fi ;;
		"${options[5]}" ) ZenityMessageRallyReport 2>/dev/null 
							if [[ $ZenityMessageRallyReport_ret == 0 ]]; then
								ViewLatestRallyReport;
							else
								echo "user pressed cancel" ;
							fi ;;
		"${options[6]}" ) ZenityMessageChangeDefaultTest 2>/dev/null 
							if [[ $ZenityMessageChangeDefaultTest_ret == 0 ]]; then
								ChangeDefaultRallyTest;
							else
								echo "user pressed cancel" ;
							fi ;;
		*) zenity --error --text="Invalid option. Try another one.";;
		esac
	done
}
#------------------------------------------------------------------------------------------------
#These intermediate functions manage the calling of the functions that perform the actual
#tasks and are required because the tasks can either be called from Zenity UI or via a parameter.
#They also alow some business logic to be integrated, without altering the purity of each task. 
#------------------------------------------------------------------------------------------------


OpenStackInstallViaDevStack(){
	InstallJQifNot ;
	CreateStackUser ;
	echo Checking if OpenStack is already installed
	openstack --version ;
	if [ $? -eq 0 ]; then
		#Inform that OpenStack is already installed and then just return
		ZenityMessageDevStackOrAnsibleAlreadyInstalled 2>/dev/null;
		return;
	fi

	InstallOpenStackViaDevStack
}


VirtualInstanceInstall(){
	InstallJQifNot ;
	CreateStackUser ;
	openstack --version ;
	if [ $? -ne 0 ]; then
		#Inform that doesent seem to be an OpenStack install
		ZenityMessageDevStackNOTAlreadyInstalled 2>/dev/null
		if [[ $ZenityMessageDevStackNOTAlreadyInstalled_ret == 0 ]]; then
			InstallOpenStackViaDevStack ;
		else
			echo There is no OpenStack installation which has been detected, however option will be given to continue
			ZenityMessageNoOpenStack 2>/dev/null;
			if [[ ! $ZenityMessageNoOpenStack_ret == 0 ]]; then
				return;
			fi
		fi
	fi
	OpenStackVirtualInstance;
}


RallyInstallWithTest(){
	InstallJQifNot ;
	CreateStackUser ;
	openstack --version ;
	if [ $? -ne 0 ]; then
		#Inform that doesent seem to be an OpenStack install
		ZenityMessageDevStackNOTAlreadyInstalled 2>/dev/null
		if [[ $ZenityMessageDevStackNOTAlreadyInstalled_ret == 0 ]]; then
			InstallOpenStackViaDevStack ;
		else
			echo There is no OpenStack installation which has been detected, however option will be given to continue
			ZenityMessageNoOpenStack 2>/dev/null;
			if [[ ! $ZenityMessageNoOpenStack_ret == 0 ]]; then
				return;
			fi
		fi
	fi
	echo Checking if Rally is already installed
	rally --version ;
	if [ $? -ne 0 ]; then
		echo Rally could not be detected so installing Rally;
		InstallRally ;
	else
		echo Rally is already installed, so straight to test ;
	fi
	RallyDefaultTest;
}


RallyInstallWithContinuousTesting(){
	InstallJQifNot ;
	CreateStackUser ;
	openstack --version ;
	if [ $? -ne 0 ]; then
		#Inform that doesent seem to be an OpenStack install
		ZenityMessageDevStackNOTAlreadyInstalled 2>/dev/null
		if [[ $ZenityMessageDevStackNOTAlreadyInstalled_ret == 0 ]]; then
			InstallOpenStackViaDevStack ;
		else
			echo There is no OpenStack installation which has been detected, however option will be given to continue
			ZenityMessageNoOpenStack 2>/dev/null;
			if [[ ! $ZenityMessageNoOpenStack_ret == 0 ]]; then
				return;
			fi
		fi
	fi
	echo Checking if Rally is already installed
	rally --version ;
	if [ $? -ne 0 ]; then
		echo Rally could not be detected so installing Rally;
		InstallRally ;
	else
		echo Rally is already installed, so straight to continuous testing ;
	fi
	RallyContinuousTesting;
}

OpenStackRallyThenTest(){
	InstallJQifNot ;
	CreateStackUser ;
	echo Checking if OpenStack is already installed
	openstack --version ;
	if [ $? -eq 0 ]; then
		echo No need to install OpenStack
	else
		InstallOpenStackViaDevStack
	fi
	echo Checking if Rally is already installed
	rally --version ;
	if [ $? -ne 0 ]; then
		echo Rally could not be detected, so installing Rally;
		InstallRally ;
	else
		echo Rally is already installed, so straight to test ;
	fi
	RallyDefaultTest;
	RallyViewReport;
}

ViewLatestRallyReport(){
	InstallJQifNot ;
	CreateStackUser ;
	echo Checking if OpenStack is already installed
	openstack --version ;
	if [ $? -eq 0 ]; then
		echo No need to install OpenStack
	else
		echo OpenStack does not seem to be installed
	fi
	echo Checking if Rally is already installed
	rally --version ;
	if [ $? -ne 0 ]; then
		echo Folder for Rally could not be detected so exiting;
		return;
	else
		echo Rally is installed, will retrieve a report ;
	fi
	RallyViewReport;
}


ChangeDefaultRallyTest(){
	echo This will launch the input box that allows you to change the default test ;
	RallyFileSelection 2>/dev/null
	RallyChangeDefaultTest 2>/dev/null
	DefineRallyDefaultTest;
	echo $RALLYTESTBANK;
}


#----------------------------------------------
#Zenity message box's called from the Zenity UI
#----------------------------------------------
ZenityMessageDEVSTACK(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "This Will install the OpenStack Environment, if it has not already been installed.\nPress \'Yes\' to continue"`</span>"
ZenityMessageDEVSTACK_ret=$?
}

ZenityMessageVirtualInstance(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "This Will install a Virtual Instance of a Linux machine on \nan existing OpenStack installation.\nPress \'Yes\' to continue"`</span>"
ZenityMessageVirtualInstance_ret=$?
}

ZenityMessageRallyWithTest(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "This will install Rally, if it is not already installed and carry out the default test.\nPress \'Yes\' to continue"`</span>"
ZenityMessageRallyWithTest_ret=$?
}

ZenityMessageRallyContinuous(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "This will install Rally, if it is not already installed and then continuously test OpenStack.\nPress \'Yes\' to continue"`</span>"
ZenityMessageRallyContinuous_ret=$?
}

ZenityMessageRallyDEVSTACKandTest(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "This will install OpenStack and then Rally, if they are not already installed and carry out the default test.\nPress \'Yes\' to continue"`</span>"
ZenityMessageRallyDEVSTACKandTest_ret=$?
}

ZenityMessageDevStackOrAnsibleAlreadyInstalled(){
zenity --warning \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "DevStack or Ansible OpenStack seems to already be installed. The install will exit.\nPress 'OK' to close the dialogue"`</span>"
ZenityMessageDevStackOrAnsibleAlreadyInstalled_ret=$?
}

ZenityMessageDevStackNOTAlreadyInstalled(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "There seems to be no OpenStack installation.\nPress \'Yes\' to install DevStack prior to continuing"`</span>"
ZenityMessageDevStackNOTAlreadyInstalled_ret=$?
}

ZenityMessageNoOpenStack(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "No OpenStack installation can be detected. Are you sure you would like to continue attempting to create a Virtual Machine instance?\nPress \'Yes\' to continue"`</span>"
ZenityMessageNoOpenStack_ret=$?
}

ZenityMessageRallyReport(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "Would you like to view the latest report produced by Rally in a browser?\nPress \'Yes\' to continue"`</span>"
ZenityMessageRallyReport_ret=$?
}

ZenityMessageChangeDefaultTest(){
zenity --question \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "Would you like to change the default test Rally uses for this session?\n\nA file dialogue will be shown and can be used to select a new json file.\nIf you cancel the file selector, the last one used will remain current.\n\nPress \'Yes\' to you wish to continue"`</span>"
ZenityMessageChangeDefaultTest_ret=$?
}

ZenityMessageNotJSON(){
zenity --error \
--text="<span color=\"#282828\" font-family=\"Arial\" font-size=\"x-large\">`printf "I don\'t think you selected a JSON file.\nPress \'OK\' to close the dialogue"`</span>"
ZenityMessageDevStackOrAnsibleAlreadyInstalled_ret=$?
}



#--------------------------------
#OpenStack install using DevStack
#--------------------------------
InstallOpenStackViaDevStack(){
	echo This will install Openstack via DevStack deployment ;
if [[ $TESTMODE == 1 ]]; then return; fi
	echo
	#Installing jq removed to function and run from internmediate launch area
	#Creating stack user removed to function and run from intermediate launch area
	echo Installing Git Core;
	echo "*******************";
	sudo -u stack -H bash -c "sudo apt install -y git-core";
	
	echo "Cloning Devstack from GIT";
	echo "*************************";
	sudo -u stack -H bash -c "cd /opt/stack; ls; 
	git clone https://git.openstack.org/openstack-dev/devstack -b stable/rocky" ;
	
	echo "Checking out from GIT";
	echo "*************************";
	sudo -u stack -H bash -c "cd /opt/stack/devstack; 
	git checkout stable/rocky ;
	cp samples/local.conf . ";
	
	echo "Launching ifconfig to view IP address";
	echo "*****************************************";
	/sbin/ifconfig ;
	#HOST_IP="10.0.2.15" ;
	
	echo "Going to extract ip with the help of AWK (from unix.stackexchange.com/questions/166999/how-to-display-the-ip-address-of-the-default-interface-with-internet-connection)";
	echo "****************************************";
	default_iface=$(awk '$2 == 00000000 { print $1 }' /proc/net/route);
	HOST_IP="$(ip addr show dev "$default_iface" | awk '$1 == "inet" { sub("/.*", "", $2); print $2 }')";
	sudo -u stack -H bash -c "cd /opt/stack/devstack; echo HOST_IP=${HOST_IP} >> local.conf" ;
	
	
	echo "installing Python pip";
	echo "*********************";
	sudo -u stack -H bash -c "cd /opt/stack/devstack; sudo apt install -y python-pip ; " ;
	
	echo "installing Python pbr";
	echo "*********************";
	sudo -u stack -H bash -c "cd /opt/stack/devstack; pip install -U pbr" ;
	
	A="--advertise-client-urls http://\$SERVICE_HOST:\$ETCD_PORT"
	B="--advertise-client-urls http://\$HOST_IP:\$ETCD_PORT"

	original=$(</opt/stack/devstack/lib/etcd3)
	newFile="${original/$A/$B}"
	sudo echo "${newFile}" > /opt/stack/devstack/lib/etcd3

	C="--listen-client-urls http://\$SERVICE_HOST:$\ETCD_PORT"
	D="--listen-client-urls http://\$HOST_IP:\$ETCD_PORT"

	original2=$(</opt/stack/devstack/lib/etcd3)
	newFile2="${original/$C/$D}"
	sudo echo "${newFile2}" > /opt/stack/devstack/lib/etcd3
	
	echo "about to run ./stack.sh";
	echo "*********************";
	sudo -u stack -H bash -c "cd /opt/stack/devstack ; ./stack.sh ";

sleep 10;
}


#------------------------------------------------------
#Virtual Instance of Linux machine created on OpenStack
#------------------------------------------------------
OpenStackVirtualInstance(){	
echo "Configuring a Virtual Instance of a Linux machine onto OpenStack"
if [[ $TESTMODE == 1 ]]; then return; fi
	echo ;
	echo "*******************************************************************";
	echo "Configuring and verifying Environmental variables";
	echo "If the shell departs from stack user, the OS Environment disappears";
	echo "*******************************************************************";
	sudo -u stack -H bash -c 'cd /opt/stack/devstack ;
	source userrc_early ;
	grep -nr “OS_USERNAME=admin” | grep rc ;
	export | grep OS_ ;
	openstack --version ;
	openstack service list ;
	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Create a public - private keypair^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	[ -e /opt/stack/.ssh/id_rsa ] && rm /opt/stack/.ssh/id_rsa ;
	[ -e /opt/stack/.ssh/id_rsa.pub ] && rm /opt/stack/.ssh/id_rsa.pub ;
	echo -ne "\n" | ssh-keygen -q -N “” ;
	openstack keypair create --public-key ~/.ssh/id_rsa.pub KP_HelloWorld ;

	echo ;	
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	echo ^Show the OpenStack Project List^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack project list ;

	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Show the Security Groups^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack security group list ;

	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^There are likely to be duplicates, which breaks Nova, so create a new one:^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack security group create adminSSH --description "Allows SSH from outside the VM." ;

	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Show the UUID for the adminSSH Security Group^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack security group list ;
	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Adding some rules for the Security Group adminSSH^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack security group rule create --ingress adminSSH ; 
	openstack security group rule create --protocol icmp adminSSH ;
	openstack security group rule create --ingress --ethertype IPv6 adminSSH ;
	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;		
	echo ^Show the Security Groups - again. Look for adminSSH^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack security group list ;
	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;		
	echo ^Show the Security Rules for adminSSH^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;		
	openstack security group rule list adminSSH ;

	
	nameSSHSG1="$(openstack security group list -f json | jq .[0].Name -r)" ;
	nameSSHSG2="$(openstack security group list -f json | jq .[1].Name -r)" ;
	nameSSHSG3="$(openstack security group list -f json | jq .[2].Name -r)" ;
	nameSSHSG4="$(openstack security group list -f json | jq .[3].Name -r)" ;
	echo $nameSSHSG1 $nameSSHSG2 $nameSSHSG3 $nameSSHSG4 ;

	if [ $nameSSHSG1 = "adminSSH" ]; then 
	zzz=0 
	fi 
	if [ $nameSSHSG2 = "adminSSH" ]; then 
	zzz=1 
	fi 
	if [ $nameSSHSG3 = "adminSSH" ]; then 
	zzz=2 
	fi 
	if [ $nameSSHSG4 = "adminSSH" ]; then 
	zzz=3 
	fi 

	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ;		
	echo ^Get the ID from the adminSSH group - and put in variable^ ;
	echo ^This is to use when opening port 22 -SSH- in the project security group                 ^ ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ ;
	adminSSHSG="$(openstack security group list -f json | jq .[$zzz].ID -r)" ;
	echo adminSSHSG ; 	
	echo $adminSSHSG ; 


	openstack security group rule create --proto tcp --dst-port 22  $adminSSHSG ;

	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Get details about the image and put them in variables^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack image list ;
	OSImage="$(openstack image list -f json | jq .[0].Name -r)" ;
	echo OSImage ; 	
	echo $OSImage ; 
	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Get details about the flavour - pick the first one -tiny-^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	openstack flavor list ;
	OSFlavour="$(openstack flavor list -f json | jq .[0].Name -r)" ;
	echo OSFlavour ;
	echo $OSFlavour ; 

	nameN1="$(openstack network list -f json | jq .[0].Name -r)" ;
	nameN2="$(openstack network list -f json | jq .[1].Name -r)" ;
	echo $nameN1 $nameN2

	if [ $nameN1 = "private" ]; then 
	zzzz=0 
	fi 
	if [ $nameN2 = "private" ]; then 
	zzzz=1 
	fi 
	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Get details about the Network ID -sourced the first one - private network-^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack network list ;
	OSNetwork_ID="$(openstack network list -f json | jq .[$zzzz].ID -r)" ;
	echo OSNetwork_ID ; 
	echo $OSNetwork_ID ; 

	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Creating the virtual instance^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack server create --flavor $OSFlavour --image $OSImage --nic net-id=$OSNetwork_ID --security-group adminSSH --key-name KP_HelloWorld VM_HelloWorld ;

	echo waiting 60 seconds for active
	sleep 60 ;
	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Verify the server has been created and has a status of Active^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack server list ;
	
	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Create a floating IP address on the network^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	OSFloating_IP="$(openstack floating ip create public -f json | jq .floating_ip_address -r)" ;
	echo OSFloating_IP ;
	echo $OSFloating_IP ;

	sleep 30;

	echo ;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Associate the floating IP address to the virtual instance^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack server add floating ip VM_HelloWorld $OSFloating_IP ;
	
	echo ;
	echo ^Verify the floating IP address has been assigned^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;	
	openstack server list ; ' ;
	
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Connecting to the virtual instance - First SSH^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo Feature not implemented yet
	echo "You will need to log in as stack: sudo su - stack"
	echo "You can then ssh cirros@Floating_IP - where Floating_IP is reported just earlier "
	echo "Type \'Yes\' if required"
	echo "Press Enter for \'passphrase for key\'"
	echo "And give it the password cubswin:)"
	echo "Check where you are: whoami"
	echo "Then you can try echo HelloWorld!"
	#echo -ne "cubswin:)" | echo -ne "yes\n" | ssh cirros@$OSFloating_IP; 
	#echo HelloWorld ; ' ;
	#echo -ne "cubswin:)" | echo -ne "\n" | ssh cirros@172.24.4.5;
}



#-------------
#Rally install
#-------------
InstallRally(){
echo This will install Rally;
if [[ $TESTMODE == 1 ]]; then return; fi
#	wget -q -O- https://raw.githubusercontent.com/openstack/rally/stable/0.12/install_rally.sh | bash;
#	. /opt/stack/rally/bin/activate; 
	sudo -u stack -H bash -c "	echo ;		
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Installing Rally and the Existing Plugin^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	sudo pip install rally-openstack;
	
	echo ;	
	echo ^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Create Rally Database^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^;
	rally db create
	
	echo ;	
	echo ^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Checking pip versions^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^;
	pip --version; 
	whoami;

	echo ;		
	echo ^^^^^^^^^^^^^^;
	echo ^List Plugins^;
	echo ^^^^^^^^^^^^^^;
	rally plugin list --platform openstack;
	
	echo ;	
	echo ^^^^^^^^^^^^^^^^^;
	echo ^Sourcing openrc^;
	echo ^^^^^^^^^^^^^^^^^;
	. /opt/stack/devstack/openrc admin admin;  
	
	echo ;	
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Create Rally deployment^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^;
	rally deployment create --fromenv --name=existing;
	
	echo ;	
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^Obtain the sample Rally Tests^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	cd ~/devstack;
	git clone https://github.com/openstack/rally-openstack.git ; " ;
	
}

#------------------
#Rally default test
#------------------
RallyDefaultTest(){
echo Performing the default Rally Test
if [[ $TESTMODE == 1 ]]; then return; fi
sudo -u stack -H bash -c "echo ;	
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	echo ^About to run the default Rally Test. first check version^;
	echo ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^;
	rally --version ;
	rally task start /opt/stack/devstack/rally-openstack/samples/tasks$RallyJSONFile ; ";
	echo rally task start /opt/stack/devstack/rally-openstack/samples/tasks$RallyJSONFile
}


#----------------------
#Rally continuous tests
#----------------------
RallyContinuousTesting(){
echo This will use Rally to perform specified or continuous number of test of OpenStack
if [[ $TESTMODE == 1 ]]; then return; fi
echo "************************************************************";
echo "Rally will be running bank $RALLYTESTBANK of tests, each separated by $RALLYTESTSEPARATION seconds and repeated $RALLYTESTREPEATS times";
echo "************************************************************";
ContinuousCount=0
#If RALLYTESTREPEATS is 0 loop will continue indefinately
for ((var = 1; var <= $RALLYTESTREPEATS || RALLYTESTREPEATS == 0 ; var++)); do
	ContinuousCount=$(($ContinuousCount + 1))
	echo "***************************************"
	echo "Rally Continuous Test number is $ContinuousCount" ;
	echo "***************************************"
	sudo -u stack -H bash -c "rally task start /opt/stack/devstack/rally-openstack/samples/tasks$RallyJSONFile ; " ;
	echo Firefox browser killed every 10th time
	#wmctrl use from https://superuser.com/questions/583246/can-i-close-firefox-browser-tab-or-firefox-browser-from-ubuntu-terminal
	if [ `expr $ContinuousCount % 10` -eq 0 ]; then 
		echo About to kill firefox non-gracefully
		sudo pkill -f firefox 
	fi
	RallyViewReport
	sleep $RALLYTESTSEPARATION ;
done
}


#----------------------
#Rally View Report
#----------------------
RallyViewReport(){
echo This will allow you to change the default test which rally uses for this session only
if [[ $TESTMODE == 1 ]]; then return; fi
sudo -u stack -H bash -c "sudo xhost +SI:localuser:stack ; 
	rally task report --out=/opt/stack/report1.html --open ; " ;
}


#----------------------
#Rally Change Default Test
#----------------------
RallyChangeDefaultTest(){
echo This will allow you to change the default test which rally uses for this session only
#--entry-text $RALLYTESTBANK
if [[ $TESTMODE == 1 ]]; then return; fi
if [ -z "$RALLYFILE" ]; then RALLYFILE=$RALLYTESTBANK ;fi
	RALLYTESTBANKOUT=$(zenity --entry --title="Change the Default Test" --entry-text ${RALLYFILE} --text="`printf "To change the default test, enter a number between 1 and 5\nor paste the path to a test: \n(Path is after the... ${RallyDefaultJSONFilePathBeginning} ...prefix)\n1- ${RallyDefaultJSONFile1}\n2- ${RallyDefaultJSONFile2}\n3- ${RallyDefaultJSONFile3}\n4- ${RallyDefaultJSONFile4}\n5- ${RallyDefaultJSONFile5}\nThen press \'OK\' to continue"`");
	#RALLYTESTBANK=$?;
	case $? in
         0) echo "\"$RALLYTESTBANKOUT\" exists."
					RALLYTESTBANK=$RALLYTESTBANKOUT ;;
         1) echo "No RALLYTESTBANKOUT";;
        -1) echo "An unexpected error has occurred.";;
	esac
	echo $RALLYTESTBANK
}

#----------------------
#File Selection Dialogue
#----------------------
RallyFileSelection(){
echo This will allow you to browse for more tests to copy  to use when changing the default test
if [[ $TESTMODE == 1 ]]; then return; fi
	RALLYFILETEMP=$(zenity --file-selection --title="Select a JSON File or Cancel for a default choice" --filename=/opt/stack/devstack/rally-openstack/samples/tasks )

case $? in
         0) echo "\"$RALLYFILETEMP\" selected."
			JSONExtension=${RALLYFILETEMP:(-4)}
			echo $JSONExtension
			if [[ $JSONExtension = "JSON" ]]; then
				RallyFileLength=${#RALLYFILETEMP}
				RALLYFILE=${RALLYFILETEMP:(-($RallyFileLength-49))}
				echo $RALLYFILE
			else
				RALLYFILE=$RALLYTESTBANK
				ZenityMessageNotJSON 2>/dev/null ;
				echo I don\'t think you selected a JSON file
				
			fi;;
         1) echo "No file selected."
			RALLYFILE=$RALLYTESTBANK;;
        -1) echo "An unexpected error has occurred."
			RALLYFILE=$RALLYTESTBANK;;
esac

}


#--------------------------------------------------
#Test for if jq is installed prior to installing it
#--------------------------------------------------
InstallJQifNot(){
	type jq;
	if [[ ! $? == 0 ]]; then
		echo "******************************";
		echo "Installing jq for parsing json";
		echo "******************************";
		echo -ne '\n' | sudo apt install jq
	fi
}



#---------------------------------------
#Create the stack user if does not exist
#---------------------------------------
CreateStackUser(){
	echo Check if the stack user exists and create if it does not
	user_exists=$(id -u stack > /dev/null 2>&1; echo $?);
	if [[ ! $user_exists == 0 ]]; then
		echo "***************************************************************";	
		echo "Creating stack user with no password and adding to sudoers list";
		echo "***************************************************************";
		sudo useradd -s /bin/bash -d /opt/stack -m stack ;
		echo "stack ALL=(ALL) NOPASSWD: ALL" | sudo tee /etc/sudoers.d/stack ;
	fi
	return;
}

#---------------------------------------
#Analyse the switch passed to the script
#---------------------------------------
case "$1" in
	-i) InteractiveInstall ;; #Interactive UI will be run.
	-o) OpenStackInstallViaDevStack ;; # This will install Openstack
	-v) VirtualInstanceInstall ;; #This will instantiate a Linux machine on OpenStack
	-r) RallyInstallWithTest ;; # This will install Rally and complete the default tests
	-c) RallyInstallWithContinuousTesting ;; # This will install Rally and continuously test OpenStack
	-a) OpenStackRallyThenTest ;; # All-In-One install of Devstack and Rally. This is the same as using switches -o and then -r
	-h) ViewLatestRallyReport ;; # View the latest Report in HTML produced by Rally
    *) InteractiveInstall ;; # In case you typed a different option other than a,b,c
esac
return;

